package ee.ioc.phon.dictationapp;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.EventQueue;
import java.awt.KeyEventDispatcher;
import java.awt.KeyboardFocusManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.PreparedStatement;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import javax.imageio.stream.FileImageInputStream;
import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.TargetDataLine;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.ScrollPaneConstants;
import javax.swing.UIManager;
import javax.swing.border.LineBorder;

import ee.ioc.phon.netspeechapi.duplex.RecognitionEvent;
import ee.ioc.phon.netspeechapi.duplex.RecognitionEvent.Result;
import ee.ioc.phon.netspeechapi.duplex.RecognitionEventListener;
import ee.ioc.phon.netspeechapi.duplex.WsDuplexRecognitionSession;

import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.SwingConstants;

import java.awt.FlowLayout;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;

import javax.swing.JTextField;

import java.awt.Insets;

import javax.swing.border.MatteBorder;
import javax.swing.border.BevelBorder;
import javax.swing.border.EmptyBorder;

import org.apache.commons.cli.BasicParser;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.OptionBuilder;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.lang.StringUtils;

import javax.swing.AbstractAction;
import javax.swing.Action;

public class DictationApp {
	

	
	private JFrame frame;
	private boolean dictating;

	private JTextArea textPane;

	private JButton dictateButton;
	private JPanel textPanel;
	private JPanel panel_1;
	private JComboBox recognizerCombo;
	private JLabel lblTuvastaja;
	private JLabel lblNewLabel;
	private JPanel panel_2;
	private JLabel lblNewLabel_1;
	private JTextField userTextField;
	private JLabel lblNewLabel_2;
	private JTextField contentTextField;
	private JButton btnSaadaTekst;
	JFileChooser fc = new JFileChooser();
	private File simulateFile = null;
	Map<String, String> recognizersMap;
	
    private class MyDispatcher implements KeyEventDispatcher {
        public boolean dispatchKeyEvent(KeyEvent e) {
            if (e.getID() == KeyEvent.KEY_PRESSED) {
            	if ((e.getKeyCode() == KeyEvent.VK_S) && e.isControlDown() && e.isAltDown() ) {
            		if (fc.showOpenDialog(frame) == JFileChooser.APPROVE_OPTION) {
            			simulateFile = fc.getSelectedFile();
            		}
            	}
            	
            }
            return false;
        }
    }	

	/**
	 * Create the application.
	 */
	public DictationApp(Map<String, String> recognizersMap) {
		this.recognizersMap = recognizersMap;
		initialize();
		for (String recName : recognizersMap.keySet()) {
			recognizerCombo.addItem(recName);
		}
		recognizerCombo.setSelectedIndex(0);
		KeyboardFocusManager manager = KeyboardFocusManager.getCurrentKeyboardFocusManager();
        manager.addKeyEventDispatcher(new MyDispatcher());		
	}

	
	
	
	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 499, 345);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(new BorderLayout(0, 0));
		
		JPanel panel = new JPanel();
		frame.getContentPane().add(panel, BorderLayout.SOUTH);
		panel.setLayout(new BorderLayout(0, 0));
		
		dictateButton = new JButton("Dikteeri");
		dictateButton.setMnemonic('d');
		dictateButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (!dictating) {
					textPane.setEditable(false);
					startDictating();
					dictateButton.setText("Stopp");
					textPanel.setBorder(new LineBorder(Color.RED, 2));
				} else {
					stopDictating();
					dictateButton.setEnabled(false);
				}
			}
		});
		dictateButton.setAlignmentY(Component.BOTTOM_ALIGNMENT);
		panel.add(dictateButton);
		
		textPanel = new JPanel();
		textPanel.setBorder(new LineBorder(UIManager.getColor("Button.background"), 2));
		frame.getContentPane().add(textPanel, BorderLayout.CENTER);
		textPanel.setLayout(new BorderLayout(0, 0));
		
		textPane = new JTextArea();
		textPane.setLineWrap(true);
		textPane.setWrapStyleWord(true);
		JScrollPane scrollPane = new JScrollPane(textPane);
		textPanel.add(scrollPane);
		scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		
		panel_1 = new JPanel();
		scrollPane.setColumnHeaderView(panel_1);
		
		panel_1.setLayout(new BorderLayout(0, 0));
		
		lblTuvastaja = new JLabel("Tuvastaja: ");
		lblTuvastaja.setLabelFor(recognizerCombo);
		panel_1.add(lblTuvastaja, BorderLayout.WEST);

		recognizerCombo = new JComboBox();
		panel_1.add(recognizerCombo, BorderLayout.CENTER);
		
		
		lblNewLabel = new JLabel(" Lisaväljad...");
		lblNewLabel.setForeground(Color.BLUE);
		lblNewLabel.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		lblNewLabel.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				setExtraPanelVisible();
			}


		});
		
		panel_1.add(lblNewLabel, BorderLayout.EAST);

		
		panel_2 = new JPanel();
		panel_2.setVisible(false);
		panel_2.setBorder(new EmptyBorder(5, 5, 5, 5));
		panel_1.add(panel_2, BorderLayout.SOUTH);
		GridBagLayout gbl_panel_2 = new GridBagLayout();
		gbl_panel_2.columnWidths = new int[]{0, 0, 0};
		gbl_panel_2.rowHeights = new int[]{0, 0, 0, 0};
		gbl_panel_2.columnWeights = new double[]{0.0, 1.0, Double.MIN_VALUE};
		gbl_panel_2.rowWeights = new double[]{0.0, 0.0, 0.0, Double.MIN_VALUE};
		panel_2.setLayout(gbl_panel_2);
		
		lblNewLabel_1 = new JLabel("Kasutaja ID: ");
		GridBagConstraints gbc_lblNewLabel_1 = new GridBagConstraints();
		gbc_lblNewLabel_1.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel_1.anchor = GridBagConstraints.EAST;
		gbc_lblNewLabel_1.gridx = 0;
		gbc_lblNewLabel_1.gridy = 0;
		panel_2.add(lblNewLabel_1, gbc_lblNewLabel_1);
		
		userTextField = new JTextField();
		GridBagConstraints gbc_userTextField = new GridBagConstraints();
		gbc_userTextField.insets = new Insets(0, 0, 5, 0);
		gbc_userTextField.fill = GridBagConstraints.HORIZONTAL;
		gbc_userTextField.gridx = 1;
		gbc_userTextField.gridy = 0;
		panel_2.add(userTextField, gbc_userTextField);
		userTextField.setColumns(10);
		
		lblNewLabel_2 = new JLabel("Teksti ID: ");
		GridBagConstraints gbc_lblNewLabel_2 = new GridBagConstraints();
		gbc_lblNewLabel_2.anchor = GridBagConstraints.EAST;
		gbc_lblNewLabel_2.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel_2.gridx = 0;
		gbc_lblNewLabel_2.gridy = 1;
		panel_2.add(lblNewLabel_2, gbc_lblNewLabel_2);
		
		contentTextField = new JTextField();
		GridBagConstraints gbc_contentTextField = new GridBagConstraints();
		gbc_contentTextField.insets = new Insets(0, 0, 5, 0);
		gbc_contentTextField.fill = GridBagConstraints.HORIZONTAL;
		gbc_contentTextField.gridx = 1;
		gbc_contentTextField.gridy = 1;
		panel_2.add(contentTextField, gbc_contentTextField);
		contentTextField.setColumns(10);
		
		btnSaadaTekst = new JButton("Saada tekst");
		btnSaadaTekst.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int confirm = JOptionPane.showConfirmDialog(btnSaadaTekst, "Saadan parandatud teksti serverisse?", "Kinnitus", JOptionPane.YES_NO_OPTION);
	            if (confirm == JOptionPane.YES_OPTION) {
	            	try {
		            	sendReferenceText();
		            	JOptionPane.showMessageDialog(btnSaadaTekst, "Tekst saadetud serverisse.");
	            	} catch (Exception e) {
	            		JOptionPane.showMessageDialog(btnSaadaTekst, "Tekst saatmine ebaõnnestus: " + e.getMessage());
	            	}
	            }
			}


		});
		GridBagConstraints gbc_btnSaadaTekst = new GridBagConstraints();
		gbc_btnSaadaTekst.insets = new Insets(0, 0, 0, 5);
		gbc_btnSaadaTekst.gridx = 0;
		gbc_btnSaadaTekst.gridy = 2;
		panel_2.add(btnSaadaTekst, gbc_btnSaadaTekst);
	}
	
	private void setExtraPanelVisible() {
		panel_2.setVisible(true);
		lblNewLabel.setVisible(false);
	}	
	
	private void sendReferenceText() throws IOException {
		String sendUrl = "http://" + getRecognizerAddress() + "/dynamic/reference";
		URL url = new URL(sendUrl);
		HttpURLConnection connection = (HttpURLConnection) url.openConnection();
		connection.setRequestMethod("POST");
		connection.setRequestProperty("Content-Id", contentTextField.getText());
		connection.setRequestProperty("User-Id", userTextField.getText());
		connection.setDoOutput(true);
		connection.connect();
		OutputStream os =  connection.getOutputStream();
		os.write(textPane.getText().getBytes("UTF-8"));
		os.flush();
		os.close();
		int responseCode = connection.getResponseCode();
		if (responseCode != HttpURLConnection.HTTP_OK) {
			throw new IOException(connection.getResponseMessage());
		}
	}	

	private String getRecognizerAddress() {
		String recName = (String)recognizerCombo.getSelectedItem();
		return recognizersMap.get(recName);
	}
	
	private void stopDictating() {
		dictating = false;
	}
	
	private void dictationClosed() {
		dictateButton.setEnabled(true);
		dictating = false;
		dictateButton.setText("Dikteeri");
		textPanel.setBorder(new LineBorder(UIManager.getColor("Button.background"), 2));
		textPane.setEditable(true);
	}

	private void startDictating() {
		AudioFormat format =  new AudioFormat(16000, 16, 1, true, false); 
		DataLine.Info info = new DataLine.Info(TargetDataLine.class,format);
		if (!AudioSystem.isLineSupported(info)) {
			// FIXME
			System.err.println("AudioSystem doesn't support recording?");
		}
		// Obtain and open the line.
		try {
			final WsDuplexRecognitionSession session = new WsDuplexRecognitionSession("ws://" + getRecognizerAddress() + "/ws/speech");
			String userId = userTextField.getText();
			if (userId.length() > 0) {
				session.setUserId(userId);
			}
			String contentId = contentTextField.getText();
			if (userId.length() > 0) {
				session.setContentId(contentId);
			}
			session.addRecognitionEventListener(new RecognitionEventListener() {
				int startPosition = textPane.getCaretPosition();
				int endPosition = startPosition;
				boolean doUpper = false;
				{
					String textBeforeCaret = textPane.getText().substring(0, endPosition);
					if ((textBeforeCaret.length() == 0) || textBeforeCaret.trim().endsWith(".") ||  textBeforeCaret.endsWith("\n")) {
						doUpper = true;
					}
				}
				
				public void onRecognitionEvent(RecognitionEvent event) {
					Result result = event.getResult();
					if (result != null) {
						RecognitionEvent.Hypothesis hyp = result.getHypotheses().get(0);
						if (!result.isFinal()) {
							String guiHypText = hyp.getTranscript();
							guiHypText = prettyfyHyp(guiHypText, doUpper);
							guiHypText = prependSpace(guiHypText, startPosition);
							textPane.replaceRange(guiHypText, startPosition, endPosition);
							endPosition = startPosition + guiHypText.length();
							textPane.setCaretPosition(endPosition);
						} else {
							String guiHypText = hyp.getTranscript();
							guiHypText = prettyfyHyp(guiHypText, doUpper);
							guiHypText = prependSpace(guiHypText, startPosition);
							doUpper = false;
							if (!guiHypText.endsWith("\n")) {
								//guiHypText = guiHypText + " ";
							} else {
								doUpper = true;
							}
							textPane.replaceRange(guiHypText, startPosition, endPosition);
							startPosition = startPosition + guiHypText.length();
							endPosition = startPosition;
							textPane.setCaretPosition(endPosition);
							if (hyp.getTranscript().trim().endsWith(" .")) {
								doUpper = true;
							} 
						}
					}
					
				}
				
				private String prependSpace(String guiHypText, int startPosition) {
					if (guiHypText.startsWith(".") || guiHypText.startsWith(",") 
							|| guiHypText.startsWith("!") || guiHypText.startsWith(":") || guiHypText.startsWith(")") ) {
						return guiHypText;
					}
					if (startPosition == 0) {
						return guiHypText;
					}
					if ((new HashSet<Character>(Arrays.asList('\n', '('))).contains(textPane.getText().charAt(startPosition - 1)))  {
						return guiHypText;
					}
					return " " + guiHypText;
				}

				private String prettyfyHyp(String text, boolean doCapFirst) {
					if (doCapFirst) {
						text = StringUtils.capitalize(text);
					}
					String [] tokens = text.split(" +");
					text = "";
					boolean doCapitalizeNext = false;
					for (String token : tokens) {
						if (text.length() > 0) {
							text = text + " ";
						}
						if (doCapitalizeNext) {
							text = text + StringUtils.capitalize(token);
						} else {
							text = text + token;
						}
						if (token.equals(".") || token.equals("\n")) {							
							doCapitalizeNext = true;
						} else {
							doCapitalizeNext = false;
						}						
					}
					
					text = text.replaceAll(" ([,.!:)])",  "$1");
					text = text.replaceAll("([(]) ",  "$1");
					text = text.replaceAll(" ?\\n ?",  "\n");
					return text;
				}	
				
				public void onClose() {
					dictationClosed();
					
				}
			});
			session.connect();		
			final TargetDataLine line = (TargetDataLine) AudioSystem.getLine(info);
		    line.open(format);
			Runnable recordingThread = new Runnable() {

				public void run() {
					// Assume that the TargetDataLine, line, has already
					// been obtained and opened.				
					int numBytesRead;
					byte[] data = new byte[line.getBufferSize() / 2];
					
					boolean simulate = simulateFile != null;
					InputStream is = null; 
					 
					// Begin audio capture.
					line.start();
					if (simulate) {						
						try {
							is = new FileInputStream(simulateFile);
						} catch (IOException e) { 
							throw new RuntimeException("Couldn't open simulation file", e);
						}
					}
					dictating = true;
					try {
						while (dictating) {
							// Read the next chunk of data from the TargetDataLine.
							
							numBytesRead = line.read(data, 0, data.length);
							if (simulate) {
								numBytesRead = is.read(data);
								if (numBytesRead < data.length) {
									if (numBytesRead == -1) {
										numBytesRead = 0;
										simulateFile = null;
									}
									dictating = false;
								}
							}
							session.sendChunk(Arrays.copyOf(data, numBytesRead), !dictating);
						}
					} catch (Exception e) {
					   System.err.println("Problem with websocket IO: " + e);
					}
					line.stop();
					line.close();
				}
			};
			new Thread(recordingThread).start();		    
		} catch (Exception e) {
			// FIXME
			System.err.println("Problem with dictation:" + e);
		}		
	}	

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		Options options = new Options();
		options.addOption(new Option("e",  "display extra fields by default"));
		Option recOption  = OptionBuilder.withArgName( "name=url" )
                .hasArgs(2)
                .withValueSeparator()
                .withDescription( "use recognizer <name> at <url>" )
                .create( 'R' );		
		options.addOption(recOption);
		
		CommandLineParser parser = new BasicParser();
		
	    try {
	        // parse the command line arguments
	        final CommandLine cmdLine = parser.parse( options, args );
	    	final Map<String, String> recognizersMap = new LinkedHashMap<String, String>();
	        
	        if (cmdLine.hasOption("R")) {
	        	Properties recProps = cmdLine.getOptionProperties("R");
	        	for (Map.Entry<Object, Object> entry :recProps.entrySet()) {
	        		recognizersMap.put(entry.getKey().toString(), entry.getValue().toString());
	        	}
	        } else {
	        	{
	        		recognizersMap.put("Tavaline kõne", "bark.phon.ioc.ee:82/dev/duplex-speech-api");
	        		recognizersMap.put("Radioloogia", "bark.phon.ioc.ee:82/radioloogia/duplex-speech-api");
	        		recognizersMap.put("Inglise keel", "bark.phon.ioc.ee:82/english/duplex-speech-api");
	        		recognizersMap.put("Tavaline kõne (dev)", "localhost:8888/client");
	        		recognizersMap.put("Radioloogia (dev)", "bayes:8891/client");
	        		recognizersMap.put("Local dev", "localhost:8888/client");
	        	}	        	
	        }
	        
			EventQueue.invokeLater(new Runnable() {
				public void run() {
					try {
						DictationApp window = new DictationApp(recognizersMap);
						if (cmdLine.hasOption("e")) {
							window.setExtraPanelVisible();
						}
						window.frame.setVisible(true);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			});	        
	    }
	    catch( ParseException exp ) {
	        // oops, something went wrong
	        System.err.println( "Parsing failed.  Reason: " + exp.getMessage() );
	    }		
		
	}
}



